﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Pad : MonoBehaviour, IDragHandler, IEndDragHandler {

	[SerializeField]
	private float maxDistance;
	private Vector3 defaultPos;
	private Vector3 quantity;

	public Vector3 Quantity {
		get {return quantity;}
	}

	void Start () {
		// 初期位置
		defaultPos = transform.localPosition;
	}

	void Update () {
		if (Vector3.Distance (defaultPos, transform.localPosition) > maxDistance) {
			transform.localPosition = defaultPos + Vector3.Normalize ((transform.localPosition - defaultPos)) * maxDistance;
		}

		// パッドで入力された値を0から1の間で計算
		Vector3 currentPos = transform.localPosition;
		quantity = currentPos - defaultPos;
		quantity /= maxDistance;
	}

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		// パッドの座標をドラッグされた位置にワールド座標で移動する
		// PointerEventData.positionで取得できるのはワールド座標のため
		transform.position = eventData.position;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		// タッチを離した場合パッドを初期位置に移動する
		transform.localPosition = defaultPos;
	}

	#endregion
}
