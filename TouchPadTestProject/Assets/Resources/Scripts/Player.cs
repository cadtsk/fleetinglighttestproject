﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	[SerializeField]
	private GameObject inputPad = null;
	[SerializeField]
	private float moveVel = 0.01f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (inputPad != null) {
			transform.position += new Vector3 (
				(inputPad.GetComponent<Pad> ().Quantity.x) * moveVel,
				0,
				(inputPad.GetComponent<Pad> ().Quantity.y) * moveVel);
		}
	}
}
